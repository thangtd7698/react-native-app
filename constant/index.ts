import ROUTES from './routes';
import IMGS from './imgs';
import COLORS from './colors';
import ICONS from './icons';

export {ROUTES, IMGS, COLORS, ICONS};
