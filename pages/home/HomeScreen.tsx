import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { COLORS } from '../../constant';
import Feather from 'react-native-vector-icons/Feather';

const styles = StyleSheet.create({});



const HomeScreen = () => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.bgColor,
      }}>
        <Feather name='battery' size={42} />
      <Text> Home!</Text>

    </View>
  );
};

export default HomeScreen;