
import { createBottomTabNavigator, BottomTabBar } from "@react-navigation/bottom-tabs"
import { TabBarVisibilityAnimationConfig } from "@react-navigation/bottom-tabs/lib/typescript/src/types";
import { Image } from "react-native/types";
import { COLORS } from "../constant";
import HomeScreen from "../pages/home/HomeScreen";

const Tab = createBottomTabNavigator();

const Tabs = () => {
  return (
    <Tab.Navigator
    >
      <Tab.Screen
        name="Home"
        component={HomeScreen}
      // options={{
      //   tabBarIcon: ({ focused }) => (
      //     <Image
      //       source={icons.cutlery}
      //       resizeMode="contain"
      //       style={{
      //         width: 25,
      //         height: 25,
      //         tintColor: focused ? COLORS.primary : COLORS.secondary
      //       }}
      //     />
      //   ),
      //   // tabBarButton: (props) => (
      //   //   <TabBarCustomButton
      //   //     {...props}
      //   //   />
      //   // )
      // }}
      />

      <Tab.Screen
        name="Search"
        component={HomeScreen}
      // options={{
      //   tabBarIcon: ({ focused }) => (
      //     <Image
      //       source={icons.search}
      //       resizeMode="contain"
      //       style={{
      //         width: 25,
      //         height: 25,
      //         tintColor: focused ? COLORS.primary : COLORS.secondary
      //       }}
      //     />
      //   ),
      //   // tabBarButton: (props) => (
      //   //   <TabBarCustomButton
      //   //     {...props}
      //   //   />
      //   // )
      // }}
      />
    </Tab.Navigator>
  )
};

export default Tabs;
